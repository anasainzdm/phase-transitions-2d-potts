#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

#define LENGTH 8   /*system size is LENGTH*LENGTH*/
int n=64; //number of spin points on lattice size*size
int q=2;
int mcs=100000;
int transient=10000; //number of transient steps

double spin[LENGTH][LENGTH];
double T=5;
double minT=0.5;
double change=0.1;
double  boolcluster[LENGTH][LENGTH]; //checks whether spin belongs to cluster

bool test_flip(int ix,int iy);
double total_magnetization();

void positivearray()
{
    int ix, iy;
    // int spin[LENGTH][LENGTH];
    for (iy=0;iy<LENGTH;iy++)
    {
        for (ix=0;ix<LENGTH;ix++)
        {
            spin[ix][iy]=1;
            printf("%f      ",spin[ix][iy]);
        }
        printf("\n");
        
    }
    
}

void initialize() //initializes and prints random lattice
{
    srand(time(0));
    
    int ix,iy,N;
    for (iy = (LENGTH-1); iy >= 0; iy--)
    {
        for (ix = 0; ix < LENGTH; ix++)
        {
            N= rand()%q;
            spin[ix][iy] = ((2*M_PI*N)/q);
            printf("%d   ",N);
        }
        printf("\n");
    }
}



//function used later for choosing random position

void choose_random_pos(int *ix,int *iy)
{
    *ix=0;
    *iy=0;
    //    srand(time(0));
    *ix=(rand()%LENGTH);
    *iy=(rand()%LENGTH);
    //    printf("ix is %d\n",ix);
    //    printf("iy is %d\n",iy);
    
}
//declare functions for the algorithm
void growCluster(int ix,int iy);
void tryadd(int ix,int iy);
int flip(int ix,int iy);

void ClusterAlg()
{
    int ix,iy;
    double E,M,Mabs,dE;
    //clear cluster array
    for(ix=0; ix<LENGTH;ix++)
    {
        for(iy=0;iy<LENGTH;iy++)
        {
            boolcluster[ix][iy] = 0;
        }
    }
    
    //choose a random spin and grow a cluster
    choose_random_pos(&ix,&iy);
    growCluster(ix,iy);
    for(ix=0; ix<LENGTH;ix++)
    {
        for(iy=0;iy<LENGTH;iy++)
        {
           if  (boolcluster[ix][iy] == 1)
           { if (test_flip(ix,iy))
           {    flip(ix,iy);
               E+=2*dE;
               M=total_magnetization();
               Mabs=fabs(total_magnetization());
        }
           }
        }
    }

}

void growCluster(ix,iy)
{
    boolcluster[ix][iy] = 1;
    
    int left,right,up,down;
    double e;
    //    periodic boundary conditions
    if (ix == 0)
    {
        left = LENGTH-1;
        right = 1;
    }
    else if (ix == (LENGTH-1))
    {
        left = LENGTH-2;
        right = 0;
    }
    else
    {
        left = ix-1;
        right = ix+1;
    }
    if (iy == 0)
    {
        up = 1;
        down = LENGTH-1;
    }
    else if (iy == (LENGTH-1))
    {
        up = 0;
        down = LENGTH-2;
    }
    else
    {
        up = iy+1;
        down = iy-1;
    }
    //if neighbour spin doesn't belong in cluster, try to add
    if (spin[ix][iy]==spin[left][iy] && boolcluster[left][iy]==0)
    tryadd(left,iy);
    if (spin[ix][iy]==spin[right][iy] && boolcluster[right][iy]==0)
    tryadd(right,iy);
    if (spin[ix][iy]==spin[ix][up] && boolcluster[ix][up]==0)
    tryadd(ix,up);
    if (spin[ix][iy]==spin[ix][down] && boolcluster[ix][down]==0)
    tryadd(ix,down);

}

void tryadd(ix,iy)
{
    double prob = (1-exp(-2/T));
    double r=rand()/RAND_MAX;
    if (r<prob)
    {
    
    growCluster(ix,iy);
        
    }
}


//compute energy of each position
int energy(int ix,int iy)
{
    int left,right,up,down;
    double e;
    //    periodic boundary conditions
    if (ix == 0)
    {
        left = LENGTH-1;
        right = 1;
    }
    else if (ix == (LENGTH-1))
    {
        left = LENGTH-2;
        right = 0;
    }
    else
    {
        left = ix-1;
        right = ix+1;
    }
    if (iy == 0)
    {
        up = 1;
        down = LENGTH-1;
    }
    else if (iy == (LENGTH-1))
    {
        up = 0;
        down = LENGTH-2;
    }
    else
    {
        up = iy+1;
        down = iy-1;
    }
    
    e = -1*(cos((spin[ix][iy])-(spin[left][iy]))+cos((spin[ix][iy])-(spin[right][iy]))+cos((spin[ix][iy])-(spin[ix][up]))+cos((spin[ix][iy])-(spin[ix][down])));
    return e;
}

bool test_flip(int ix,int iy)
{
    int dE;
    dE = -2*energy(ix,iy);
    //        printf("energy is %d\n",energy(ix,iy));
    //        printf("Denergy is %d\n",dE);
    double r=((double)rand()/(double)RAND_MAX); //necessary to adjust this. otherwise always testflip false
    //    printf("%f\n",r);
    if ((dE<0)||(r<exp(-dE/T))) //flip due to lower energy or heat bath
    return true;
}



//flip spin at some position
int flip(ix,iy)
{
    int N= rand()%q;
    if (((2*M_PI*N)/q)!=spin[ix][iy])
    {spin[ix][iy] = ((2*M_PI*N)/q);
        return spin[ix][iy];
    }
    else
    return flip(ix,iy);
}

//function for disregarding transient results
void transient_results()
{   int a,b,dE;
    int ix,iy;
    for(a=1;a<=transient;a++)
    {
        for(b=1;b<=n;b++)
        {
            choose_random_pos(&ix,&iy);
            
            if(test_flip(ix,iy))
            {
                flip(ix,iy);
                
            }
            
        }
    }
}

//calculating total magnetization
double total_magnetization()
{
    double m=0;
    int ix,iy;
    for (iy=LENGTH-1;iy>=0;iy--)
    {
        for (ix=0;ix<LENGTH;ix++)
        {
            m+=cos(spin[ix][iy]);
        }
    }
    return m;
}

//function to calculate total energy

int total_energy()
{
    int ix,iy;
    int E=0;
    for (iy=LENGTH-1;iy>=0;iy--)
    {
        for (ix=0;ix<LENGTH;ix++)
        {
            E+=energy(ix,iy);
        }
    }
    
    return E;
}

double jackknife(double*A,double avg)
{
    double deviation=0;
    double X_i[mcs];
    double X=0;
    double sigma = 0;
    for (int a=1;a<=mcs;a++)
    {
        if (a%100==0)
        {
            X_i[a]=((avg-A[a])/(1000-1));
            X+=(X_i[a]/1000);
            sigma+=(X_i[a]-X)* (X_i[a]-X);
            deviation = sqrt(((1000-1)*sigma)/(1000))/n;
        }
    }
    return deviation;
}



int main()
{
    double total_time;
    clock_t start, end;
    start = clock();
    FILE *fp;
    fp = fopen("datacolumn.txt","w");
    srand(time(0));
    initialize();
    double norm = (mcs*n/100) ; //normalization for averaging (mcs*n)
    double E=0,Esq=0,Esq_avg=0,E_avg=0,etot=0,etotsq=0;
    double M=0,Msq=0,Msq_avg=0,M_avg=0,mtot=0,mtotsq=0;
    double Mabs=0,Mabs_avg=0,Mq_avg=0,mabstot=0,mqtot=0;
    double C=0,X=0,U=0;

    double Edata[mcs],Esqdata[mcs],Mdata[mcs],Mabsdata[mcs],Msqdata[mcs],Mqdata[mcs];
    double E_error,Esq_error,M_error,Msq_error,Mq_error,Mabs_error;
    double C_error,X_error,U_error;
    int dE=0;
    int ix,iy;
    double prob = (1-exp(-2/T));
    
    fprintf(fp,"T           E          Eerror        Esq        Esqerror         Mabs         Mabserror       Msq        Msqerror        Mq          Mqerror        X       Xerror      U       Uerror\n");
    //Temperature loop
    for (;T>=minT;T=(T-change))
    {
        //transient results
        transient_results();
        //observables adopt equilibrium
        M=total_magnetization();
        Mabs=fabs(total_magnetization());
        E=total_energy();
        //initialize summation variables at each temperature step
        etot=0;
        etotsq=0;
        mtot=0;
        mtotsq=0;
        mabstot=0;
        mqtot=0;
        
        //Monte Carlo loop
        for(int a=1;a<=mcs;a++)
        {
                ClusterAlg();
                //        adjust observables
            Edata[a]=(E/2.0);
            Esqdata[a]=(E/2.0)*(E/2.0);
            Mdata[a]=M;
            Mabsdata[a]=(sqrt(M*M));
            Msqdata[a]=(M*M);
            Mqdata[a]=(M*M*M*M);
        //keep summation of observables
            if (a%100==0)
            {
        etot+=(E/2.0); //so as not to count the energy for each spin twice
        etotsq+=((E/2.0)*(E/2.0));
        mtot+=M;
        mtotsq+=M*M;
        mqtot+=M*M*M*M;
        mabstot+=(sqrt(M*M));
            }}
    //average observables
    E_avg= (etot/norm);
    Esq_avg=(etotsq/(n*norm));
    M_avg=(mtot/norm);
    Msq_avg=(mtotsq/(n*norm));
    Mabs_avg=(mabstot/norm);
    Mq_avg=(mqtot/(n*n*n*norm));
        
//    calculate error for observables
        E_error=jackknife(Edata,E_avg);
        Esq_error=((jackknife(Esqdata,Esq_avg))/n);
        M_error=jackknife(Mdata,M_avg);
        Mabs_error=jackknife(Mabsdata,Mabs_avg);
        Msq_error=((jackknife(Msqdata,Msq_avg))/n);
        Mq_error=((jackknife(Mqdata,Mq_avg))/(n*n*n));
        
    //        calculate C,X and U from observables and their error
    C=((Esq_avg-(E_avg*E_avg))/(T*T));
        C_error = ((1/(T*T))*(fabs(Esq_error)+fabs((2*E_avg*E_error*n))));
    X=((Msq_avg-(Mabs_avg*Mabs_avg))/(T));
        X_error = ((1/T)*(fabs(Msq_error)+fabs(2*Mabs_avg*Mabs_error*n)));
    U=(1-((Mq_avg)/(3*Msq_avg*Msq_avg)));
        U_error = (fabs(-Mq_error/(3*n*Msq_avg))+fabs((Mq_avg*Msq_error)/(3*n*Msq_avg*Msq_avg)));
        
//print list of data
        fprintf(fp,"%.2f    %lf    %lf     %lf     %lf     %lf     %lf     %lf     %lf     %lf     %lf     %lf     %lf     %lf     %lf\n" ,T,E_avg,E_error,Esq_avg,Esq_error,Mabs_avg,Mabs_error,Msq_avg,Msq_error,Mq_avg,Mq_error,X,X_error,U,U_error);
    
}
    fclose(fp);

    end = clock();
    total_time = ((double) (end - start)) / CLOCKS_PER_SEC;
    //calulate total time
    printf("\nTime taken is: %f", total_time);
}
