#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

#define LENGTH 20   /*system size is LENGTH*LENGTH*/
int n=400; //number of spin points on lattice size*size
int mcs=100000;
int transient=500; //number of transient steps
double norm = 40000000; //normalization for averaging 1/(mcs*n)
int spin[LENGTH][LENGTH];
double T=5;
double minT=0.5;
double change=0.1;


void positivearray()
{
    int ix, iy;
    // int spin[LENGTH][LENGTH];
    for (iy=0;iy<LENGTH;iy++)
    {
        for (ix=0;ix<LENGTH;ix++)
        {
            spin[ix][iy]=1;
            printf("%d      ",spin[ix][iy]);
        }
        printf("\n");
        
    }
    
}

void initialize()
{
    //       srand(time(0));
    
    int ix,iy;
    for (iy = (LENGTH-1); iy >= 0; iy--)
    {
        for (ix = 0; ix < LENGTH; ix++)
        {
            if (rand()%2==0)
                spin[ix][iy] = 1;
            else
                spin[ix][iy] = -1;
        }
    }
}

//printed lattice on screen
void lattice()
{
    int ix,iy;
    for (iy = LENGTH-1; iy >= 0; iy--)
    {
        for (ix = 0; ix < LENGTH; ix++)
        {
            if (spin[ix][iy]<0)
                printf("-   ");
            else
                printf("+   ");
        }
        printf("\n");
    }
}

//function used later for choosing random position

void choose_random_pos(int *ix,int *iy)
{
    *ix=0;
    *iy=0;
    //    srand(time(0));
    *ix=(rand()%20);
    *iy=(rand()%20);
    //    printf("ix is %d\n",*ix);
    //    printf("iy is %d\n",*iy);
    
}

//compute energy of each position
int energy(int ix,int iy)
{
    int e,left,right,up,down;
    //    periodic boundary conditions
    if (ix == 0)
    {
        left = LENGTH-1;
        right = 1;
    }
    else if (ix == (LENGTH-1))
    {
        left = LENGTH-2;
        right = 0;
    }
    else
    {
        left = ix-1;
        right = ix+1;
    }
    if (iy == 0)
    {
        up = 1;
        down = LENGTH-1;
    }
    else if (iy == (LENGTH-1))
    {
        up = 0;
        down = LENGTH-2;
    }
    else
    {
        up = iy+1;
        down = iy-1;
    }
    
    e = -1*spin[ix][iy]*(spin[left][iy]+spin[right][iy]+spin[ix][up]+spin[ix][down]);
    return e;
}

bool test_flip(int ix,int iy)
{
    int dE;
    dE = -2*energy(ix,iy);
    //        printf("energy is %d\n",energy(ix,iy));
    //        printf("Denergy is %d\n",dE);
    double r=((double)rand()/(double)RAND_MAX); //necessary to adjust this. otherwise always testflip false
    //    printf("%f\n",r);
    if ((dE<0)||(r<exp(-dE/T))) //flip due to lower energy or heat bath
        return true;
}


//flip spin at some position
int flip(ix,iy)
{
    spin[ix][iy]=-spin[ix][iy];
    return spin[ix][iy];
}

//function for disregarding transient results
void transient_results()
{   int a,b,dE;
    int ix,iy;
    for(a=1;a<=transient;a++)
    {
        for(b=1;b<=n;b++)
        {
            choose_random_pos(&ix,&iy);
            //            printf("%d and %d\n",ix,iy);
            //            printf("spin is %d \n", spin[ix][iy]);
            if(test_flip(ix,iy))
            {
                flip(ix,iy);
                //                printf("spin is %d \n", spin[ix][iy]);
            }
            
        }
    }
}

//calculating total magnetization
int total_magnetization()
{
    int m;
    int pos=0;
    int neg=0;
    int ix,iy;
    for (iy=LENGTH-1;iy>=0;iy--)
    {
        for (ix=0;ix<LENGTH;ix++)
        {
            if (spin[ix][iy]==1)
                pos+=1;
            else
                neg+=1;
        }
    }
    m=pos-neg;
    
    return m;
}

//function to calculate total energy

int total_energy()
{
    int e;
    int ix,iy;
    int E=0;
    for (iy=LENGTH-1;iy>=0;iy--)
    {
        for (ix=0;ix<LENGTH;ix++)
        {
            E+=energy(ix,iy);
        }
    }
    
    return E;
}



int main()
{
    srand(time(0));
    initialize();
    lattice();
    double E=0,Esq=0,Esq_avg=0,E_avg=0,etot=0,etotsq=0;
    double M=0,Msq=0,Msq_avg=0,M_avg=0,mtot=0,mtotsq=0;
    double Mabs=0,Mabs_avg=0,Mq_avg=0,mabstot=0,mqtot=0;
    double C=0,X=0,U=0;
    double Eavg[50],Esqavg[50],Mavg[50],Msqavg[50],Mqavg[50],Mabsavg[50];
    double Tlist[50],Clist[50],Xlist[50],Ulist[50];
  
    int dE=0;
    int ix,iy;
    int nchange=0;
    //Temperature loop
    for (;T>=minT;T=(T-change))
    {
        //transient results
        transient_results();
        
        //observables adopt equilibrium
        M=total_magnetization();
        Mabs=abs(total_magnetization());
        E=total_energy();
//        printf("E is %f\n",E);
//        printf("M is %f\n",M);
//        printf("M abs is %f\n",Mabs);
        //initialize summation variables at each temperature step
        etot=0;
        etotsq=0;
        mtot=0;
        mtotsq=0;
        mabstot=0;
        mqtot=0;
        
        //Monte Carlo loop
        for(int a=1;a<=mcs;a++)
        {
            //Metropolis loop
            for(int b=1;b<=n;b++)
            {
                choose_random_pos(&ix,&iy);
                if(test_flip(ix,iy))
                {
                    flip(ix,iy);
                    //        adjust observables
                    E+=2*dE;
                    M+=2*spin[ix][iy];
                    Mabs+=abs(spin[ix][iy]);
                } }
            //keep summation of observables
            etot+=(E/2.0); //so as not to count the energy for each spin twice
            etotsq+=((E/2.0)*(E/2.0));
            mtot+=M;
            mtotsq+=M*M;
            mqtot+=M*M*M*M;
            mabstot+=(sqrt(M*M));
        }
        //average observables
        E_avg= (etot/norm);
        Eavg[nchange]=E_avg;
        Esq_avg=(etotsq/(n*norm));
        Esqavg[nchange]=Esq_avg;
        M_avg=(mtot/norm);
        Mavg[nchange]=M_avg;
        Msq_avg=(mtotsq/(n*norm));
        Msqavg[nchange]=Msq_avg;
        Mabs_avg=(mabstot/norm);
        Mabsavg[nchange]=Mabs_avg;
        Mq_avg=(mqtot/(n*n*n*norm));
        Mqavg[nchange]=Mq_avg;
        Tlist[nchange]=T;
        
//        calculate C,X and U from observables
        C=((Esq_avg-(pow(E_avg,2)))/(T*T));
        Clist[nchange]=C;
        X=((Msq_avg-(Mabs_avg*Mabs_avg))*n/(T));
        Xlist[nchange]=X;
        U=(1-((Mq_avg)/(3*Msq_avg)));
        Ulist[nchange]=U;
        
        
        nchange++;
    }
//    print lists of data to plot graphs
    printf("T=\n");
    for(int i=nchange-1;i>=0;i--)
    {printf("%f, ",Tlist[i]);
    }
    printf("\n");
    printf("Eavg=\n");
    for(int i=nchange-1;i>=0;i--)
    {printf("%f, ",Eavg[i]);
}
    printf("\n");
    printf("Esqavg=\n");
    for(int i=nchange-1;i>=0;i--)
    {printf("%f, ",Esqavg[i]);
    }
    printf("\n");
    printf("Mavg=\n");
    for(int i=nchange-1;i>=0;i--)
    {printf("%f, ",Mavg[i]);
    }
    printf("\n");
    printf("Msqavg=\n");
    for(int i=nchange-1;i>=0;i--)
    {printf("%f, ",Msqavg[i]);
    }
    printf("\n");
    printf("Mabsavg=\n");
    for(int i=nchange-1;i>=0;i--)
    {printf("%f, ",Mabsavg[i]);
    }
    printf("\n");
    printf("Mqavg=\n");
    for(int i=nchange-1;i>=0;i--)
    {printf("%f, ",Mqavg[i]);
    }
    printf("\n");
    printf("Heat capacity=\n");
    for(int i=nchange-1;i>=0;i--)
    {printf("%f, ",Clist[i]);
    }
    printf("\n");
    printf("Susceptibility=\n");
    for(int i=nchange-1;i>=0;i--)
    {printf("%f, ",Xlist[i]);
    }
    printf("\n");
    printf("Cumulant=\n");
    for(int i=nchange-1;i>=0;i--)
    {printf("%f, ",Ulist[i]);
    }
    printf("\n");
        }

